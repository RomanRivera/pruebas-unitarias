package Problemas;

import java.util.StringTokenizer;

public class Problemas {

    public static boolean MenorOigualAcero(int e) {
        if (e == 0 || e < 0) {
            return true;
        } else {
            return false;
        }
    }

    public static String Palabras(String a) {
        String Palabra = a;
        String Extraer = Palabra.substring(0, 2);
        String Union = Extraer + "... " + Extraer + "... " + Palabra + "?";
        return Union;
    }

    public static int ContadorPalabras(String t) {
        String Frase = t;
        StringTokenizer st = new StringTokenizer(Frase);
        return st.countTokens();
    }

    public static int Tazas(int o) {
        
        int c= 5;
        int div = o / c;
        int ro = div + o;
        return ro;

    }
}
